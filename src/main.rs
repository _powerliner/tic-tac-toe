pub mod game_board;
pub mod team;
use std::io::stdin;
fn main() {
    let mut game = game_board::GameBoard::new(3);
    let mut current_team: u32 = 2;
    while game.check_win(1) != true && game.check_win(2) != true {
        println!("{}", game);
        if current_team == 1 {
            current_team = 2;
        }
        else {
            current_team = 1;
        }

        if game.is_full() == true {
            break;
        }

        let mut input_x = String::new();
        let mut input_y = String::new();

        println!("Player {}, enter the x position to place your mark", current_team);

        stdin().read_line(&mut input_x).expect("Input error");
        input_x.pop();

        println!("Player {}, enter the y position to place your mark", current_team);

        stdin().read_line(&mut input_y).expect("Input error");
        input_y.pop();

        let x = input_x.parse::<u32>().unwrap();
        let y = input_y.parse::<u32>().unwrap();

        match game.mark(current_team, x, y) {
            true => {
            },
            false => {
                println!("Error, cannot mark at {}, {}", x, y);
                continue;
            }
        }


    }
    if game.is_full() == true {
        println!("Tie!");
    }
    else {
        println!("{}", game);
        println!("Player {} Wins!", current_team);
    }
}
