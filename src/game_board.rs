use std::fmt;

pub struct GameBoard {
    size: u32,
    board: Vec<Vec<u32>>,
    plays: u32
}

impl GameBoard {

    pub fn new(size: u32) -> GameBoard {
        let mut sanitized_size = size;
        if size < 3 {
            sanitized_size = 3;
        }
        if size > 9 {
            sanitized_size = 9;
        }

        // This simply creates a 2D vector that is size x size and zeros it
        let board_vec: Vec<Vec<u32>> = (0..sanitized_size).map(|_| { let my_vec: Vec<u32> = (0..sanitized_size).map(|_| 0).collect(); my_vec }).collect();

        GameBoard {
            size: sanitized_size,
            board: board_vec,
            plays: 0
        }
    }

    pub fn mark(&mut self, team: u32, xcoord: u32, ycoord: u32) -> bool {
        let final_xcoord = xcoord - 1;
        let final_ycoord = ycoord - 1;
        let mut set: bool = false;
        if final_ycoord < self.size && final_xcoord < self.size && xcoord >= 1 && ycoord >= 1 && self.cell_is_empty(final_xcoord, final_ycoord) == true {
            self.board[final_xcoord as usize][final_ycoord as usize] = team;
            self.plays += 1;
            set = true;
        }
        if set == false {
            return false;
        }
        true

    }

    fn cell_is_empty(&mut self, xcoord: u32, ycoord: u32) -> bool {
        if self.board[xcoord as usize][ycoord as usize] != 0 {
            return false;
        }
        true
    }

    #[allow(dead_code)]
    pub fn is_full(&mut self) -> bool {
        if self.plays == (self.size.pow(2)) {
            return true;
        }
        false
    }

    #[allow(dead_code)]
    fn is_empty(&mut self) -> bool {
        if self.plays == 0 {
            return true;
        }
        false
    }


    // Checks for win given team so that we can either end the game or add points to a team
    pub fn check_win(&mut self, team: u32) -> bool {
        let mut num_consec_h: u32 = 0;
        let mut num_consec_v: u32 = 0;
        let mut num_consec_d: u32 = 0;

        // So because this is tic-tac-toe, you can win by either getting "board size" in a row
        // horizontally, vertically, or diagonally. So we gotta check for all of these conditions

        // Check win horizontally
        for y in 0..self.size {
            for x in 0..self.size {
                if self.board[x as usize][y as usize] == team {
                    num_consec_h += 1;
                }
                else {
                    num_consec_h = 0;
                }
                if num_consec_h == self.size {
                    return true;
                }

            }
        }
        // Check win vertically
        for x in 0..self.size {
            for y in 0..self.size {
                if self.board[x as usize][y as usize] == team {
                    num_consec_v += 1;
                }
                else {
                    num_consec_v = 0;
                }
                if num_consec_v == self.size {
                    return true;
                }

            }
        }

        // Check win diagonally
        for x in 0..self.size {
            let y = x;
            if self.board[x as usize][y as usize] == team {
                num_consec_d += 1;
            }
        }
        if num_consec_d == self.size {
            return true;
        }
        num_consec_d = 0;
        for y in 0..self.size {
            let x = self.size - 1 - y;
            if self.board[x as usize][y as usize] == team {
                num_consec_d += 1;
            }
        }
        if num_consec_d == self.size {
            return true;
        }

        false

    }

    pub fn get_size(&mut self) -> u32 {
        self.size
    }

    pub fn get_plays(&mut self) -> u32 {
        self.plays
    }

}

impl fmt::Display for GameBoard {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut output_string = String::new();
        output_string.push_str(" ");
        for i in 0..self.size {
            let mut x_num: String = String::from(" ");
            x_num.push_str((i + 1).to_string().as_str());
            output_string.push_str(x_num.as_str());
        }
        output_string.push_str("\n -");
        for _ in 0..self.size {
            output_string.push_str("--");
        }
        output_string.push_str("\n");
        for y in 0..self.size {
            output_string.push_str((y + 1).to_string().as_str());
            output_string.push_str("|");
            for x in 0..self.size {
                match self.board[x as usize][y as usize] {
                    0 => {
                        output_string.push_str(" |");
                    },
                    1 => {
                        output_string.push_str("X|");
                    },
                    2 => {
                        output_string.push_str("O|");
                    },
                    _ => {
                        output_string.push_str("?|");
                    },
                }
            }
            output_string.push_str("\n -");
            for _ in 0..self.size {
                output_string.push_str("--");
            }
            output_string.push_str("\n");
        }
        output_string.push_str("\n");
        write!(f, "{}", output_string)
    }

}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn invalid_board_placement() {
        let mut my_game: GameBoard = GameBoard::new(5);
        assert!(my_game.mark(1, 5, 6) == false);
    }

    #[test]
    fn place_board_low() {
        let mut my_game: GameBoard = GameBoard::new(5);
        assert!(my_game.mark(1, 1, 1));

    }

    #[test]
    fn place_board_high() {
        let mut my_game: GameBoard = GameBoard::new(7);
        assert!(my_game.mark(1, 7, 7));
    }

    #[test]
    fn board_size_truncate_high() {
        let mut my_game: GameBoard = GameBoard::new(11);
        assert!(my_game.get_size() == 9);
    }

    #[test]
    fn board_size_truncate_low() {
        let mut my_game: GameBoard = GameBoard::new(1);
        assert!(my_game.get_size() == 3);
    }

    #[test]
    fn check_win_diag_1() {
        let mut my_game: GameBoard = GameBoard::new(4);
        my_game.mark(1, 1, 1);
        my_game.mark(1, 2, 2);
        my_game.mark(1, 3, 3);
        my_game.mark(1, 4, 4);

        assert!(my_game.check_win(1));
    }
    #[test]
    fn check_win_diag_2() {
        let mut my_game: GameBoard = GameBoard::new(4);
        my_game.mark(1, 4, 1);
        my_game.mark(1, 3, 2);
        my_game.mark(1, 2, 3);
        my_game.mark(1, 1, 4);

        assert!(my_game.check_win(1));
    }

    #[test]
    fn check_win_v_1() {
        let mut my_game: GameBoard = GameBoard::new(4);
        my_game.mark(1, 4, 1);
        my_game.mark(1, 4, 2);
        my_game.mark(1, 4, 3);
        my_game.mark(1, 4, 4);
        assert!(my_game.check_win(1));
    }

    #[test]
    fn check_win_h_1() {
        let mut my_game: GameBoard = GameBoard::new(4);
        my_game.mark(1, 1, 3);
        my_game.mark(1, 2, 3);
        my_game.mark(1, 3, 3);
        my_game.mark(1, 4, 3);
        assert!(my_game.check_win(1));
    }

    #[test]
    fn mark_nonempty() {
        let mut my_game: GameBoard = GameBoard::new(4);
        my_game.mark(1, 1, 3);
        assert!(my_game.mark(1, 1, 3) != true);
        assert!(my_game.mark(2, 1, 3) != true);
    }

    #[test]
    fn is_board_empty() {
        let mut my_game: GameBoard = GameBoard::new(4);
        assert!(my_game.is_empty());
        my_game.mark(1, 1, 1);
        assert!(my_game.is_empty() != true);
    }

    #[test]
    fn is_board_full() {
        let mut my_game: GameBoard = GameBoard::new(3);
        my_game.mark(1, 1, 1);
        my_game.mark(1, 1, 2);
        my_game.mark(1, 1, 3);
        my_game.mark(1, 2, 1);
        my_game.mark(1, 2, 2);
        my_game.mark(1, 2, 3);
        my_game.mark(1, 3, 1);
        my_game.mark(1, 3, 2);
        my_game.mark(1, 3, 3);
        assert!(my_game.is_full());
    }

}
