use std::fmt;
pub enum WhichTeam {
    TeamX,
    TeamO,
}


pub struct Team {
    points: u32,
}

impl Team {

    pub fn new() -> Team {
        Team {
            points: 0,
        }
    }

    pub fn set_points(&mut self, newpoints: u32) {
        self.points = newpoints;
    }

    pub fn reset(&mut self) {
        self.points = 0;
    }

    pub fn add_point(&mut self) {
        self.points += 1;
    }

    pub fn get_points(&mut self) -> u32 {
        self.points
    }

}

impl fmt::Display for Team {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.points)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // Make sure points are added
    #[test]
    fn add_points() {
        let mut team: Team = Team::new();
        for _ in 0..5 {
            team.add_point();
        }
        assert!(team.get_points() == 5);
    }

    // Make sure reset works
    #[test]
    fn reset_points() {
        let mut team: Team = Team::new();
        team.reset();

        assert!(team.get_points() == 0);
    }

    // Make sure set works
    #[test]
    fn set_points() {
        let mut team: Team = Team::new();
        team.set_points(5);
        assert!(team.get_points() == 5);

        team.set_points(122032);
        assert!(team.get_points() == 122032);
    }

}
