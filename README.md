## Tic-tac-toe for the command line, written in rust

### Build dependencies
 - Cargo (Tested on 1.58.0)
 - Rustc (Tested on 1.58.0)

### Supported Operating Systems
 - Only supported and tested on 64bit Linux (but really it's tic tac toe, it probably runs on >= Windows 8, recent MacOS, and recent BSD too)

### Build Instructions (Linux 64bit)
`git clone https://gitlab.com/_powerliner/tic-tac-toe.git` \
`cd tic-tac-toe` \
`cargo build --release`

- You will find the resulting binary in target/release
- Build instructions for other operating systems will vary. If you want to run this program on another operating system and can't figure out how to build, please open an issue and I'll add build instructions for your operating system :)


### Planned features
 - [ ] Better UX, including multiple rounds (best of 3/5?), Less painful way to mark the board
 - [ ] Computer opponent
